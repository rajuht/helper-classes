// All Variable Here
var templatedir = "./",
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    minifyCss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();



// Compile Scss to Css
gulp.task('compile-sass', function () {
    return gulp.src(templatedir + 'scss/helper.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(templatedir + 'css/'));
});

// Css Autoprefixer and Minify
gulp.task('prefixMinify', ['compile-sass'], function () {
    return gulp.src(templatedir + 'css/helper.css')
        .pipe(autoprefixer({
            browsers: ['last 4 versions']
        }).on('error', function (error) {
            console.log(error);
        }))
        .pipe(gulp.dest(templatedir + 'css')).on('end', function () {
            gulp.src(templatedir + 'css/helper.css')
                .pipe(minifyCss().on('error', function (err) {
                    console.log(err);
                }))
                .pipe(rename({
                    suffix: '.min'
                }))
                .pipe(gulp.dest(templatedir + 'css'));
        });
});

// Task to Watch Changes
gulp.task('watch-project', ['prefixMinify'], function () {
    //watch Scss file
    gulp.watch(templatedir + 'scss/**/*.scss', ['prefixMinify']);

    //browser sync
    gulp.watch(templatedir + 'scss/**/*.scss').on('change', browserSync.reload);
    gulp.watch(templatedir + '*.html').on('change', browserSync.reload);
});


// Browser Sync
gulp.task('browser-sync', function () {
    browserSync.init({
        //proxy: "http://localhost/htmlthemes/ruby/src/"
        server: './' 
    });
});

// Task When Running on Terminal
gulp.task('default', ['watch-project', 'browser-sync']);